package client

import client.model.*
import io.rsocket.RSocket
import io.rsocket.core.RSocketClient
import io.rsocket.metadata.WellKnownMimeType
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.io.Resource
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.core.io.buffer.DefaultDataBuffer
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.core.serializer.DefaultDeserializer
import org.springframework.http.MediaType
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.retrieveAndAwait
import org.springframework.messaging.rsocket.retrieveFlow
import org.springframework.util.MimeTypeUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.nio.file.Path
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.util.MimeType
import java.nio.ByteBuffer
import java.util.function.Function
import kotlin.random.Random


@SpringBootApplication
class UploadClient(
	@Value("classpath:input/20210315_024017_StatusAndIssue.mp4")
	private val resource: Resource? = null
) : CommandLineRunner {

	val logger: Logger = LogManager.getLogger(UploadClient::class.java)
	final val kMegabyte = 1000 * 1000

	val randomDataBuffer = DefaultDataBufferFactory(true).wrap(
		Random.nextBytes(ByteArray(kMegabyte), 1, kMegabyte)
	)

    override fun run(vararg args: String?) {

		runBlocking {
			var chunkNumber = 0L
			CscRSocketClient.requester()
				.route("upload")
				.metadata(Foo("mike"), MimeTypeUtils.APPLICATION_JSON)
				// Normally, I would use DataBufferUtils to chunk the data...
				.data(randomDataBuffer)
				.retrieveFlow<Foo>()
				//.retrieveFlow<String>()
				.collect {
					chunkNumber++
						logger.debug(">>>>>>>> Client Received Confirmation: $chunkNumber blocks,")
				}
		}
    }
}

fun main(args: Array<String>) {
    runApplication<UploadClient>(*args)
}
