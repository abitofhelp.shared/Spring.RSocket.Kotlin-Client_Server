package client.model

import com.fasterxml.jackson.databind.JsonSerializer
import io.rsocket.RSocket
import io.rsocket.core.Resume
import io.rsocket.frame.decoder.PayloadDecoder
import io.rsocket.metadata.WellKnownMimeType
import org.springframework.context.annotation.Bean
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.codec.*
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.http.codec.cbor.Jackson2CborDecoder
import org.springframework.http.codec.cbor.Jackson2CborEncoder
import org.springframework.http.codec.json.*
import org.springframework.http.codec.protobuf.ProtobufDecoder
import org.springframework.http.codec.protobuf.ProtobufEncoder
import org.springframework.http.codec.xml.Jaxb2XmlDecoder
import org.springframework.http.codec.xml.Jaxb2XmlEncoder
import org.springframework.http.codec.xml.XmlEventDecoder
import org.springframework.messaging.rsocket.*
import org.springframework.util.MimeType
import org.springframework.util.MimeTypeUtils
import org.springframework.web.util.pattern.PathPatternRouteMatcher
import reactor.core.publisher.Mono
import reactor.util.retry.Retry
import java.lang.reflect.ParameterizedType
import java.time.Duration

private const val kInitialDataBufferCapacity = 8192

private const val kFixedDelayTime = 1L
private const val kMaxKeepAliveLifeTime = 120L
private const val kMaxReconnectRetryAttempts = 10L
private const val kMaxReconnectTime = 5L
private const val kMaxResumeAttempts = 60L
private const val kMinBackoffTime = 1L
private const val kTimeBetweenKeepAliveFrames = 500L

private const val kHost = "localhost"
private const val kPort = 6565

object CscRSocketClient {

    suspend fun requester(): RSocketRequester {

        fun rSocketStrategies(): RSocketStrategies {
            return RSocketStrategies.builder()
                .dataBufferFactory(DefaultDataBufferFactory(true, kInitialDataBufferCapacity))
                .addEncoders()
                .addDecoders()
                .addMetadataExtractors()
                .routeMatcher(PathPatternRouteMatcher())
                .build()
        }

        return RSocketRequester.builder()
            .rsocketStrategies(rSocketStrategies())
            .rsocketConnector { connector ->
                connector.payloadDecoder(PayloadDecoder.ZERO_COPY)
                connector.resume(
                    Resume()
                        .sessionDuration(Duration.ofMinutes(kMaxReconnectTime))
                        .retry(Retry.fixedDelay(kMaxResumeAttempts, Duration.ofSeconds(kFixedDelayTime)))
                )
                connector.reconnect(Retry.backoff(kMaxReconnectRetryAttempts, Duration.ofSeconds(kMinBackoffTime)))
                connector.keepAlive(Duration.ofMillis(kTimeBetweenKeepAliveFrames), Duration.ofSeconds(kMaxKeepAliveLifeTime))
            }
            .dataMimeType(MimeTypeUtils.APPLICATION_OCTET_STREAM)
            .metadataMimeType(MimeType.valueOf(WellKnownMimeType.MESSAGE_RSOCKET_COMPOSITE_METADATA.toString())) //"message/x.rsocket.composite-metadata.v0"))
            .connectTcpAndAwait(kHost, kPort)
    }

    private fun RSocketStrategies.Builder.addEncoders(): RSocketStrategies.Builder {
        this.encoders { encoders: MutableList<Encoder<*>?> ->
            encoders.addAll(
                listOf(
                    ByteArrayEncoder(),
                    ByteBufferEncoder(),
                    CharSequenceEncoder.allMimeTypes(),
                    DataBufferEncoder(),
                    Jackson2CborEncoder(),
                    Jackson2JsonEncoder(),
                    //Jackson2SmileEncoder(),
                    //Jaxb2XmlEncoder(),
                    KotlinSerializationJsonEncoder(),
                    NettyByteBufEncoder(),
                    //ProtobufEncoder(),
                    ResourceEncoder(),
                    ResourceRegionEncoder()
                )
            )
        }

        return this
    }

    private fun RSocketStrategies.Builder.addDecoders(): RSocketStrategies.Builder {
        this.decoders { decoders: MutableList<Decoder<*>?> ->
            decoders.addAll(
                listOf(
                    ByteArrayDecoder(),
                    ByteBufferDecoder(),
                    DataBufferDecoder(),
                    Jackson2CborDecoder(),
                    Jackson2JsonDecoder(),
                    //Jackson2SmileDecoder(),
                    //Jaxb2XmlDecoder(),
                    KotlinSerializationJsonDecoder(),
                    NettyByteBufDecoder(),
                    //ProtobufDecoder(),
                    ResourceDecoder(),
                    StringDecoder.allMimeTypes(),
                    //XmlEventDecoder()
                )
            )
        }

        return this
    }

    private fun RSocketStrategies.Builder.addMetadataExtractors(): RSocketStrategies.Builder {
        this.metadataExtractorRegistry { registry: MetadataExtractorRegistry ->
            registry.metadataToExtract<Foo>(MimeTypeUtils.APPLICATION_JSON, "foo")
        }

        return this
    }
}