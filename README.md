# Spring-RSocket-Kotlin: Request-Stream example with pojo metadata

This repository contains a work-in-progress in which I am trying 
to transfer a Flow<DataBuffer> stream with POJO metadata from the 
client to the server.  The server emits a block number for each
chunk that has been processed, so the client can verify that 
all is well.  The client sending the stream of data to the
server is working properly, but sending metadata results in 

"Caused by: java.lang.IllegalArgumentException: No decoder for client.model.Foo
at org.springframework.messaging.rsocket.RSocketStrategies.decoder".

I am unable to pass a POJO from the client to the server, despite
trying what was in the documentation.  I hope that someone
can give me an idea as to what is wrong.

Here is the exception that is thrown:

java.lang.IllegalStateException: Failed to execute CommandLineRunner
at org.springframework.boot.SpringApplication.callRunner(SpringApplication.java:809) ~[spring-boot-2.4.4.jar:2.4.4]
at org.springframework.boot.SpringApplication.callRunners(SpringApplication.java:790) ~[spring-boot-2.4.4.jar:2.4.4]
at org.springframework.boot.SpringApplication.run(SpringApplication.java:333) ~[spring-boot-2.4.4.jar:2.4.4]
at org.springframework.boot.SpringApplication.run(SpringApplication.java:1313) ~[spring-boot-2.4.4.jar:2.4.4]
at org.springframework.boot.SpringApplication.run(SpringApplication.java:1302) ~[spring-boot-2.4.4.jar:2.4.4]
at client.UploadClientKt.main(UploadClient.kt:77) ~[main/:na]
Caused by: java.lang.IllegalArgumentException: No decoder for client.model.Foo
at org.springframework.messaging.rsocket.RSocketStrategies.decoder(RSocketStrategies.java:90) ~[spring-messaging-5.3.5.jar:5.3.5]
at org.springframework.messaging.rsocket.DefaultRSocketRequester$DefaultRequestSpec.retrieveFlux(DefaultRSocketRequester.java:314) ~[spring-messaging-5.3.5.jar:5.3.5]
at org.springframework.messaging.rsocket.DefaultRSocketRequester$DefaultRequestSpec.retrieveFlux(DefaultRSocketRequester.java:300) ~[spring-messaging-5.3.5.jar:5.3.5]
at client.UploadClient$run$1.invokeSuspend(UploadClient.kt:77) ~[main/:na]
at kotlin.coroutines.jvm.internal.BaseContinuationImpl.resumeWith(ContinuationImpl.kt:33) ~[kotlin-stdlib-1.4.31.jar:1.4.31-release-344 (1.4.31)]
at kotlinx.coroutines.DispatchedTask.run(DispatchedTask.kt:106) ~[kotlinx-coroutines-core-jvm-1.4.3.jar:na]
at kotlinx.coroutines.EventLoopImplBase.processNextEvent(EventLoop.common.kt:274) ~[kotlinx-coroutines-core-jvm-1.4.3.jar:na]
at kotlinx.coroutines.BlockingCoroutine.joinBlocking(Builders.kt:84) ~[kotlinx-coroutines-core-jvm-1.4.3.jar:na]
at kotlinx.coroutines.BuildersKt__BuildersKt.runBlocking(Builders.kt:59) ~[kotlinx-coroutines-core-jvm-1.4.3.jar:na]
at kotlinx.coroutines.BuildersKt.runBlocking(Unknown Source) ~[kotlinx-coroutines-core-jvm-1.4.3.jar:na]
at kotlinx.coroutines.BuildersKt__BuildersKt.runBlocking$default(Builders.kt:38) ~[kotlinx-coroutines-core-jvm-1.4.3.jar:na]
at kotlinx.coroutines.BuildersKt.runBlocking$default(Unknown Source) ~[kotlinx-coroutines-core-jvm-1.4.3.jar:na]
at client.UploadClient.run(UploadClient.kt:56) ~[main/:na]
at org.springframework.boot.SpringApplication.callRunner(SpringApplication.java:806) ~[spring-boot-2.4.4.jar:2.4.4]
... 5 common frames omitted
