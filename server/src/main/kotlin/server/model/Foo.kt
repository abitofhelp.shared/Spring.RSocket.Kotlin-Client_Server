package server.model

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.io.IOException

@JsonDeserialize(using = FooDeserializer::class)
data class Foo(val name: String)

// JSON Deserializer
class FooDeserializer :
    StdDeserializer<Foo?>(Foo::class.java) {
    @Throws(IOException::class, JsonProcessingException::class)

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Foo {
        val tree: JsonNode = p.codec.readTree(p)
        return Foo(tree.textValue())
    }
}