package server.controllers

import server.model.Foo
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.messaging.handler.annotation.*
import org.springframework.stereotype.Controller
import reactor.core.publisher.Mono
import java.lang.Exception

@Controller
class ServerController {
    val logger: Logger = LogManager.getLogger(ServerController::class.java)

    @MessageMapping("upload")
    suspend fun upload(@Header metadata: Foo, @Payload flow: Flow<DataBuffer>) = flow {

        var blockCounter = 0L
        var totalBytes = 0L

        flow.collect { dataBuffer: DataBuffer ->
            val length = dataBuffer.readableByteCount().toLong()
            totalBytes += length
            ++blockCounter
            logger.debug(">>>>> Server Received: $blockCounter blocks, ${length}/${totalBytes} bytes")
            // Send the number of the block that was just processed to the client/requester.
            emit(blockCounter.toString())
        }
    }
}