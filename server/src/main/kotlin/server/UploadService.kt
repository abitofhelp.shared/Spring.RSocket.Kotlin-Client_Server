package server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class UploadService

fun main(args: Array<String>) {
	runApplication<UploadService>(*args)
}
